﻿#include <iostream>
#include <vector>
#include <thread>

#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

pthread_mutex_t mtx; // мутекс
pthread_barrier_t barrier; // барьер
long numberOfChatterboxes; // количество болтунов

/// <summary>
/// Класс, описывающий болтуна.
/// </summary>
class Chatterbox
{
public:
	bool isAvailable = true; // доступен ли болтун для звонка

	Chatterbox() {}

	Chatterbox(long id)
	{
		pthread_mutex_init(&mutex, NULL);
		talkTime = rand() % 4 + 1;
		availableNumberOfCalls = rand() % 4 + 1;
		isAvailable = rand() % 2;
		this->id = id;
	}

	long getId() {
		return id;
	}

	int getAvailableNumberOfCalls() {
		return availableNumberOfCalls;
	}

	/// <summary>
	/// Совершает звонок другому болтуну.
	/// </summary>
	/// <param name="chatterbox">Болтун, которому совершается звонок</param>
	void makeACall(Chatterbox& chatterbox)
	{
		isTalking = false;

		pthread_mutex_lock(&mtx);
		std::cout << "Chatterbox_" << id << " is calling " << chatterbox.id << " . . ." << std::endl;
		pthread_mutex_unlock(&mtx);

		chatterbox.pickUpThePhone(*this);
		isTalking = false;
		availableNumberOfCalls--;
		isAvailable = rand() % 2;

		if (isAvailable) {
			pthread_mutex_lock(&mtx);
			std::cout << "Chatterbox_" << id << " is waiting for a call now . . ." << std::endl;
			pthread_mutex_unlock(&mtx);
		}
	}

	/// <summary>
	/// Принимает звонок от другого болтуна.
	/// </summary>
	/// <param name="caller">Болтун, который совершает звонок</param>
	void pickUpThePhone(Chatterbox& caller)
	{
		pthread_mutex_lock(&mutex);

		caller.isTalking = true;
		isTalking = true;
		int time = talkTime;
		if (caller.talkTime > time) {
			time = caller.talkTime;
		}

		pthread_mutex_lock(&mtx);
		std::cout << "Chatterbox_" << id << " picked up the phone from " << caller.id << " and starts a conversation for " << time << " seconds . . ." << std::endl;
		pthread_mutex_unlock(&mtx);

		std::this_thread::sleep_for(std::chrono::seconds(time));
		isTalking = false;

		pthread_mutex_unlock(&mutex);
	}

private:
	long id; // номер болтуна по списку
	int talkTime; // время разговора
	bool isTalking = false; // разговаривает ли в данный момент болтун
	int availableNumberOfCalls; // оставшееся количество звонков

	pthread_mutex_t mutex; // локальный мутекс
};

// Вектор болтунов.
std::vector<Chatterbox> chatterboxes;

size_t firstActiveTalker(size_t index)
{
	for (int i = 0; i < chatterboxes.size(); i++) {
		if (index != i && chatterboxes[i].isAvailable) {
			return i;
		}
	}

	return -1;
}

void* call(void* param)
{
	Chatterbox* currentTalker = (Chatterbox*)param;
	pthread_barrier_wait(&barrier);

	while (currentTalker->getAvailableNumberOfCalls() > 0) {
		if (!currentTalker->isAvailable) {
			size_t index;
			int count = 0;
			do {
				if (count > 100) {
					index = firstActiveTalker(currentTalker->getId());
					if (index == -1 || index == currentTalker->getId()) {
						std::cout << "Chatterbox " << currentTalker->getId() << " is the only chatterbox left with available number of calls." << std::endl;
						return NULL;
					}
					break;
				}
				srand((currentTalker->getId() + 1) * (count + 1) + currentTalker->getAvailableNumberOfCalls());
				index = rand() % chatterboxes.size();
				count++;
			} while (!chatterboxes[index].isAvailable || chatterboxes[index].getAvailableNumberOfCalls() == 0 || index == currentTalker->getId());

			currentTalker->makeACall(chatterboxes[index]);
		}
		else {
			return NULL;
		}
	}

	pthread_mutex_lock(&mtx);
	std::cout << "Chatterbox_" << currentTalker->getId() << " has no more available calls." << std::endl;
	pthread_mutex_unlock(&mtx);

	currentTalker->isAvailable = true;
	return NULL;
}

int main(int argc, char** argv)
{
	std::cout << "Enter the number of chatterboxes: ";
	std::cin >> numberOfChatterboxes;

	if (numberOfChatterboxes < 3) {
		std::cout << "The number of chatterboxes must be more than 2 . . ." << std::endl;
		return -1;
	}

	// Вектор потоков.
	std::vector<pthread_t> threads(numberOfChatterboxes);

	srand(time(NULL));
	pthread_barrier_init(&barrier, nullptr, numberOfChatterboxes);
	pthread_mutex_init(&mtx, NULL);

	// Заполняем вектор болтунов.
	for (size_t i = 0; i < numberOfChatterboxes; i++) {
		chatterboxes.push_back(Chatterbox(i));
	}

	// Создание потоков.
	for (size_t i = 0; i < numberOfChatterboxes; i++) {
		pthread_create(&threads[i], NULL, call, &chatterboxes[i]);
	}

	// Завершение работы всех потоков.
	for (size_t i = 0; i < numberOfChatterboxes; i++) {
		pthread_join(threads[i], nullptr);
	}

	std::cout << "Chatterboxes finished their calls!" << std::endl;

	return 0;
}